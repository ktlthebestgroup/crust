#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "vm.h"
#include "instructions.h"
#include "registers.h"
#include <memory>
#include <vector>
#include <sstream>

using instruction_ptr = std::shared_ptr< Instruction >;

TEST_CASE( "Check registers configuration" ){
  REQUIRE( RAX == 0 );
  REQUIRE( RBX == 1 );
  REQUIRE( RCX == 2 );
  REQUIRE( RDX == 3 );
  REQUIRE( EIP == 4 );
  REQUIRE( STP == 5 );
  REQUIRE( BFP == 6 );
  REQUIRE( CFL == 7 );
  REQUIRE( REGN == 8 );
}

TEST_CASE( "Check condflags configuration" ){
  REQUIRE( ZERO == 1 );
  REQUIRE( POSV == 2 );
  REQUIRE( NEGV == 4 );
}

TEST_CASE( "Check registers function" ){
  std::vector< uint16_t > regs( REGN );
  REQUIRE( check_registers_empty( regs ) == true );

  int i = 2;
  for( auto &val : regs ){
    val = i; i += 2;
  }

  REQUIRE( check_registers_empty( regs ) == false );
  clear_registers( regs );
  REQUIRE( check_registers_empty( regs ) == true );
}

TEST_CASE( "Simple machine operations" ){
  GIVEN( "An empty virtual machine" ){
    Machine m;
    WHEN( "Nothing is done" ){
      THEN( "Machine is empty" ){
        REQUIRE( m. inst_size() == 0 );
        REQUIRE( m. registers_empty() );
      }
    }

    WHEN( "Chaning registers from registers_state" ){
      m.registers_state()[RAX] = 2;
      THEN( "Actual registers don't change" ){
        REQUIRE( m.registers_state()[RAX] == 0 );
      }
    }
  }
}

TEST_CASE( "Testing instructions" ){
  Machine m;

  SECTION( "Testing comments" ){
    auto inst = std::shared_ptr< Comment >( new Comment( "Hello VM!" ) );
    m. add_inst( inst );
    REQUIRE( m.inst_size() == 1 );
    std::stringstream buff;
    buff << m;
    auto val = buff.str();
    REQUIRE( val == "Bytecode:\n[0]: comment: Hello VM!\n\nBytecode size: 1" );
  }

  WHEN( "Instructions are instantiated" ){
    auto load1 = std::shared_ptr< Load >( new Load( RAX, 1 ) );
    REQUIRE( load1 -> value() == 1 );

    THEN( "Add instruction into vector for loading" ){
      m. add_inst( load1 );
      REQUIRE( m. inst_size() == 1 );
      REQUIRE( m. registers_empty() );

      SECTION( "Running the machine" ){
        WHEN( "Machine is run" ){
          m. run();
          THEN( "The register is set" ){
            REQUIRE( m.registers_state()[ RAX ] == 1 );
          }
        }
      }

      SECTION( "A bit more complex example" ){
        GIVEN( "A new load instruction"){
          auto load2 = std::shared_ptr< Load >( new Load( RBX, 2 ) );
          m. add_inst( load2 );
          REQUIRE( m.inst_size() == 2 );

          WHEN( "An ADD RAX + RBX and run machine" ){
            auto add1 = std::shared_ptr< Add >( new Add( RAX, RBX ) );
            m. add_inst( add1 );
            m. run();
            THEN( "RAX is 3 and RBX is 2" ){
              REQUIRE( m. registers_state()[ RAX ] == 3 );
              REQUIRE( m. registers_state()[ RBX ] == 2 );
            }
          }

          WHEN( "An ADD RBX + RAX and run machine" ){
            auto add1 = std::shared_ptr< Add >( new Add( RBX, RAX ) );
            m. add_inst( add1 );
            m. run();
            THEN( "RAX is 1 and RBX is 3" ){
              REQUIRE( m. registers_state()[ RAX ] == 1 );
              REQUIRE( m. registers_state()[ RBX ] == 3 );
            }
          }
        }
      }
    }
  }

  WHEN( "ALL of registers are set to 0" ){
    REQUIRE( m. registers_empty() );
    THEN( "Instantiate all registers with load instructions" ){
      auto load1 = std::shared_ptr< Load >( new Load( RAX, 1 ) );
      auto load2 = std::shared_ptr< Load >( new Load( RBX, 2 ) );
      auto load3 = std::shared_ptr< Load >( new Load( RCX, 3 ) );
      auto load4 = std::shared_ptr< Load >( new Load( RDX, 4 ) );
      auto load6 = std::shared_ptr< Load >( new Load( STP, 6 ) );
      auto load7 = std::shared_ptr< Load >( new Load( BFP, 7 ) );
      auto load8 = std::shared_ptr< Load >( new Load( CFL, 8 ) );

      std::vector< instruction_ptr > vect = {
        load1,
        load2,
        load3,
        load4,
        load6,
        load7,
        load8,
      };

      m. load( vect );
      m. run();

      for( size_t i = 0; i < REGN; ++ i ){
        if( i == 4 ) continue;
        std::string msg = std::string( "Register number " ) + char(i + '1') + std::string( "is equal to ") + char(i + '1');
        SECTION( msg ){
          REQUIRE( m.registers_state()[i] == i + 1 );
        }
      }
    }
  }

  SECTION( "Testing CMP command" ){
    WHEN( "RAX and RBX are set to equal" ){
      auto load1 = std::shared_ptr< Load >( new Load( RAX, 1 ) );
      auto load2 = std::shared_ptr< Load >( new Load( RBX, 1 ) );
      auto cmp = std::shared_ptr< Compare >( new Compare( RAX, RBX ) );
      std::vector< instruction_ptr > inst = { load1, load2, cmp };
      m. load( inst );
      m.run();
      THEN( "ZERO flag is set and no other" ){
        REQUIRE( m. registers_state()[ CFL ] == ZERO );
      }
    }

    WHEN( "RAX is bigger than RBX" ){
      auto load1 = std::shared_ptr< Load >( new Load( RAX, 10 ) );
      auto load2 = std::shared_ptr< Load >( new Load( RBX, 2 ) );
      auto cmp = std::shared_ptr< Compare >( new Compare( RAX, RBX ) );
      std::vector< instruction_ptr > inst = { load1, load2, cmp };
      m. load( inst );
      m.run();
      THEN( "POSV flag is set and no other" ){
        REQUIRE( m. registers_state()[ CFL ] == POSV );
      }
    }

    WHEN( "RAX is bigger than RBX" ){
      auto load1 = std::shared_ptr< Load >( new Load( RAX, -2 ) );
      auto load2 = std::shared_ptr< Load >( new Load( RBX, -10 ) );
      auto cmp = std::shared_ptr< Compare >( new Compare( RAX, RBX ) );
      std::vector< instruction_ptr > inst = { load1, load2, cmp };
      m. load( inst );
      m.run();
      THEN( "POSV flag is set and no other" ){
        REQUIRE( m. registers_state()[ CFL ] == POSV );
      }
    }

    WHEN( "RAX is less than RBX" ){
      auto load1 = std::shared_ptr< Load >( new Load( RAX, 2 ) );
      auto load2 = std::shared_ptr< Load >( new Load( RBX, 10 ) );
      auto cmp = std::shared_ptr< Compare >( new Compare( RAX, RBX ) );
      std::vector< instruction_ptr > inst = { load1, load2, cmp };
      m. load( inst );
      m.run();
      THEN( "NEGV flag is set and no other" ){
        REQUIRE( m. registers_state()[ CFL ] == NEGV );
      }
    }

    WHEN( "RAX is less than RBX (neg. edition)" ){
      auto load1 = std::shared_ptr< Load >( new Load( RAX, -10 ) );
      auto load2 = std::shared_ptr< Load >( new Load( RBX, -2 ) );
      auto cmp = std::shared_ptr< Compare >( new Compare( RAX, RBX ) );
      std::vector< instruction_ptr > inst = { load1, load2, cmp };
      m. load( inst );
      m.run();
      THEN( "NEGV flag is set and no other" ){
        REQUIRE( m. registers_state()[ CFL ] == NEGV );
      }
    }
  }

  SECTION( "Testing arithmetic operations" ){
    auto load1 = std::shared_ptr< Load >( new Load( RAX, 5 ) );
    auto load2 = std::shared_ptr< Load >( new Load( RBX, 7 ) );

    auto add12 = std::shared_ptr< Add >( new Add( RAX, RBX ) );
    auto add21 = std::shared_ptr< Add >( new Add( RBX, RAX ) );

    auto sub12 = std::shared_ptr< Sub >( new Sub( RAX, RBX ) );
    auto sub21 = std::shared_ptr< Sub >( new Sub( RBX, RAX ) );

    auto mul12 = std::shared_ptr< Mul >( new Mul( RAX, RBX ) );
    auto mul21 = std::shared_ptr< Mul >( new Mul( RBX, RAX ) );

    auto div12 = std::shared_ptr< Div >( new Div( RAX, RBX ) );
    auto div21 = std::shared_ptr< Div >( new Div( RBX, RAX ) );

    auto mod12 = std::shared_ptr< Mod >( new Mod( RAX, RBX ) );
    auto mod21 = std::shared_ptr< Mod >( new Mod( RBX, RAX ) );

    auto and12 = std::shared_ptr< And >( new And( RAX, RBX ) );
    auto and21 = std::shared_ptr< And >( new And( RBX, RAX ) );

    auto or12 = std::shared_ptr< Or >( new Or( RAX, RBX ) );
    auto or21 = std::shared_ptr< Or >( new Or( RBX, RAX ) );

    auto xor12 = std::shared_ptr< XOr >( new XOr( RAX, RBX ) );
    auto xor21 = std::shared_ptr< XOr >( new XOr( RBX, RAX ) );

    auto lsh12 = std::shared_ptr< LShift >( new LShift( RAX, RBX ) );
    auto lsh21 = std::shared_ptr< LShift >( new LShift( RBX, RAX ) );

    auto rsh12 = std::shared_ptr< RShift >( new RShift( RAX, RBX ) );
    auto rsh21 = std::shared_ptr< RShift >( new RShift( RBX, RAX ) );

    m. add_inst( load1 );
    m. add_inst( load2 );

    WHEN( "RAX + RBX is performed" ){
      m. add_inst( add12 );
      m. run();
      THEN( "RAX is equal to sum, while the RBX is the same" ){
        REQUIRE( m. registers_state()[ RAX ] == 12 );
        REQUIRE( m. registers_state()[ RBX ] == 7 );
      }
    }

    WHEN( "RBX + RAX is performed" ){
      m. add_inst( add21 );
      m. run();
      THEN( "RBX is equal to sum, while the RAX is the same" ){
        REQUIRE( m. registers_state()[ RBX ] == 12 );
        REQUIRE( m. registers_state()[ RAX ] == 5 );
      }
    }

    WHEN( "RAX - RBX is performed" ){
      m. add_inst( sub12 );
      m. run();
      THEN( "RAX is equal to difference, while the RBX is the same" ){
        REQUIRE( m. registers_state()[ RAX ] == (uint16_t)(-2) );
        REQUIRE( m. registers_state()[ RBX ] == 7 );
      }
    }

    WHEN( "RBX - RAX is performed" ){
      m. add_inst( sub21 );
      m. run();
      THEN( "RBX is equal to difference, while the RAX is the same" ){
        REQUIRE( m. registers_state()[ RBX ] == 2 );
        REQUIRE( m. registers_state()[ RAX ] == 5 );
      }
    }

    WHEN( "RAX * RBX is performed" ){
      m. add_inst( mul12 );
      m. run();
      THEN( "RAX is equal to product, while the RBX is the same" ){
        REQUIRE( m. registers_state()[ RAX ] == 35 );
        REQUIRE( m. registers_state()[ RBX ] == 7 );
      }
    }

    WHEN( "RBX * RAX is performed" ){
      m. add_inst( mul21 );
      m. run();
      THEN( "RBX is equal to product, while the RAX is the same" ){
        REQUIRE( m. registers_state()[ RBX ] == 35 );
        REQUIRE( m. registers_state()[ RAX ] == 5 );
      }
    }

    WHEN( "RAX / RBX is performed" ){
      m. add_inst( div12 );
      m. run();
      THEN( "RAX is equal to division, while the RBX is the same" ){
        REQUIRE( m. registers_state()[ RAX ] == 0 );
        REQUIRE( m. registers_state()[ RBX ] == 7 );
      }
    }

    WHEN( "RBX / RAX is performed" ){
      m. add_inst( div21 );
      m. run();
      THEN( "RBX is equal to division, while the RAX is the same" ){
        REQUIRE( m. registers_state()[ RBX ] == 1 );
        REQUIRE( m. registers_state()[ RAX ] == 5 );
      }
    }

    WHEN( "RAX % RBX is performed" ){
      m. add_inst( mod12 );
      m. run();
      THEN( "RAX is equal to modulus, while the RBX is the same" ){
        REQUIRE( m. registers_state()[ RAX ] == 5 );
        REQUIRE( m. registers_state()[ RBX ] == 7 );
      }
    }

    WHEN( "RBX % RAX is performed" ){
      m. add_inst( mod21 );
      m. run();
      THEN( "RBX is equal to modulus, while the RAX is the same" ){
        REQUIRE( m. registers_state()[ RBX ] == 2 );
        REQUIRE( m. registers_state()[ RAX ] == 5 );
      }
    }

    WHEN( "RAX & RBX is performed" ){
      m. add_inst( and12 );
      m. run();
      THEN( "RAX is equal to bitwise-and, while the RBX is the same" ){
        auto val = 5 & 7;
        REQUIRE( m. registers_state()[ RAX ] == val );
        REQUIRE( m. registers_state()[ RBX ] == 7 );
      }
    }

    WHEN( "RBX & RAX is performed" ){
      m. add_inst( and21 );
      m. run();
      THEN( "RBX is equal to bitwise-and, while the RAX is the same" ){
        auto val = 5 & 7;
        REQUIRE( m. registers_state()[ RBX ] == val );
        REQUIRE( m. registers_state()[ RAX ] == 5 );
      }
    }

    WHEN( "RAX | RBX is performed" ){
      m. add_inst( or12 );
      m. run();
      THEN( "RAX is equal to bitwise-or, while the RBX is the same" ){
        auto val = 5 | 7;
        REQUIRE( m. registers_state()[ RAX ] == val );
        REQUIRE( m. registers_state()[ RBX ] == 7 );
      }
    }

    WHEN( "RBX | RAX is performed" ){
      m. add_inst( or21 );
      m. run();
      THEN( "RBX is equal to bitwise-or, while the RAX is the same" ){
        auto val = 5 | 7;
        REQUIRE( m. registers_state()[ RBX ] == val );
        REQUIRE( m. registers_state()[ RAX ] == 5 );
      }
    }

    WHEN( "RAX ^ RBX is performed" ){
      m. add_inst( xor12 );
      m. run();
      THEN( "RAX is equal to bitwise-xor, while the RBX is the same" ){
        auto val = 5 ^ 7;
        REQUIRE( m. registers_state()[ RAX ] == val );
        REQUIRE( m. registers_state()[ RBX ] == 7 );
      }
    }

    WHEN( "RBX ^ RAX is performed" ){
      m. add_inst( xor21 );
      m. run();
      THEN( "RBX is equal to bitwise-xor, while the RAX is the same" ){
        auto val = 5 ^ 7;
        REQUIRE( m. registers_state()[ RBX ] == val );
        REQUIRE( m. registers_state()[ RAX ] == 5 );
      }
    }

    WHEN( "RAX << RBX is performed" ){
      m. add_inst( lsh12 );
      m. run();
      THEN( "RAX is equal to left-shift, while the RBX is the same" ){
        REQUIRE( m. registers_state()[ RAX ] == 5 << 7 );
        REQUIRE( m. registers_state()[ RBX ] == 7 );
      }
    }

    WHEN( "RBX << RAX is performed" ){
      m. add_inst( lsh21 );
      m. run();
      THEN( "RBX is equal to left-shift, while the RAX is the same" ){
        REQUIRE( m. registers_state()[ RBX ] == 7 << 5 );
        REQUIRE( m. registers_state()[ RAX ] == 5 );
      }
    }

    WHEN( "RAX >> RBX is performed" ){
      m. add_inst( rsh12 );
      m. run();
      THEN( "RAX is equal to right-shift, while the RBX is the same" ){
        REQUIRE( m. registers_state()[ RAX ] == 0 );
        REQUIRE( m. registers_state()[ RBX ] == 7 );
      }
    }

    WHEN( "RBX >> RAX is performed" ){
      m. add_inst( rsh21 );
      m. run();
      THEN( "RBX is equal to right-shift, while the RAX is the same" ){
        REQUIRE( m. registers_state()[ RBX ] == 0 );
        REQUIRE( m. registers_state()[ RAX ] == 5 );
      }
    }
  }
}

TEST_CASE( "Testing function calls and stack operations" ){
  Machine m;

  WHEN( "Consequtive jumps are done" ){
    auto jump1 = std::shared_ptr< Jump >( new Jump( 1 ) );
    auto jump2 = std::shared_ptr< Jump >( new Jump( 2 ) );
    auto jump3 = std::shared_ptr< Jump >( new Jump( 3 ) );
    auto jump4 = std::shared_ptr< Jump >( new Jump( 4 ) );
    auto jump5 = std::shared_ptr< Jump >( new Jump( 5 ) );
    auto jump6 = std::shared_ptr< Jump >( new Jump( 6 ) );
    auto jump7 = std::shared_ptr< Jump >( new Jump( 7 ) );
    auto load1 = std::shared_ptr< Load >( new Load( RAX, 1005 ) );

    std::vector< std::shared_ptr< Instruction >> inst = {
      jump1,
      jump2,
      jump3,
      jump4,
      jump5,
      jump6,
      jump7,
      load1,
    };

    m. load( inst );
    m. run();

    THEN( "The action count should be correct" ){
      REQUIRE( m. registers_state()[ RAX ] == 1005 );
      REQUIRE( m. get_action_count() == 8 );
    }
  }

  WHEN( "Swapping using push and pop" ){
    auto load1 = std::shared_ptr< Load >( new Load( RAX, 5 ) );
    auto load2 = std::shared_ptr< Load >( new Load( RBX, 3 ) );
    auto push1 = std::shared_ptr< Push >( new Push( RAX ) );
    auto push2 = std::shared_ptr< Push >( new Push( RBX ) );
    auto pop1  = std::shared_ptr< Pop >( new Pop( RAX ) );
    auto pop2  = std::shared_ptr< Pop >( new Pop( RBX ) );

    std::vector< std::shared_ptr< Instruction >> inst = { load1, load2, push1, push2, pop1, pop2 };
    m. load( inst );
    m. run();

    THEN( "Values should be swapped" ){
      REQUIRE( m. registers_state()[ RAX ] == 3 );
      REQUIRE( m. registers_state()[ RBX ] == 5 );
    }
  }

  WHEN( "Jump is performed given" ){
    auto load1 = std::shared_ptr< Load >( new Load( RAX, 1 ) );
    auto jump  = std::shared_ptr< Jump >( new Jump( 3 )      ); // NOP index
    auto load2 = std::shared_ptr< Load >( new Load( RAX, 5 ) );
    auto nop   = std::shared_ptr< Nop > ( new Nop()          );
    
    std::vector< std::shared_ptr< Instruction >> inst = { load1, jump, load2, nop };
    m. load( inst );
    m. run();

    THEN( "Second load must be skipped" ){
      REQUIRE( m. registers_state()[ RAX ] == 1 );
      REQUIRE_FALSE( m. registers_state()[ RAX ] == 5 );
    }
  }

  WHEN( "Call is perfomed" ){
    auto jump_to_main  = std::shared_ptr< Jump >( new Jump( 3 ) ); // NOP index
    auto load = std::shared_ptr< Load >( new Load( RAX, 1 ) );
    auto ret = std::shared_ptr< Return >( new Return() );
    auto call = std::shared_ptr< Call >( new Call( 1 ) );

    std::vector< std::shared_ptr< Instruction >> inst = { jump_to_main, load, ret, call };
    m. load( inst );
    m. run();

    THEN( "The return call is performed correctly" ){
      REQUIRE( m. registers_state()[ RAX ] == 1 );
      REQUIRE( m. registers_state()[ STP ] == 0 );
      REQUIRE( m. registers_state()[ BFP ] == 0 );
    }
  }

  WHEN( "Loading some value from stack address" ){
    auto load = std::shared_ptr< Load >( new Load( RAX, 19 ) );
    auto push = std::shared_ptr< Push >( new Push( RAX ) );
    auto load_from_stack = std::shared_ptr< LoadFromStack >( new LoadFromStack( RBX, 0 ) );
    auto drop = std::shared_ptr< Drop >( new Drop() );

    std::vector< std::shared_ptr< Instruction >> inst = { load, push, load_from_stack, drop };
    m. load( inst );
    m. run();

    THEN( "The value should load there correctly" ){
      REQUIRE( m. registers_state()[ RBX ] == 19 );
      REQUIRE( m. registers_state()[ STP ] == 0 );
    }
  }

  WHEN( "Dumping some value onto stack address" ){
    auto load = std::shared_ptr< Load >( new Load( RAX, 19 ) );
    auto dump = std::shared_ptr< DumpIntoStack >( new DumpIntoStack( RAX, 0 ) );
    auto load_from_stack = std::shared_ptr< LoadFromStack >( new LoadFromStack( RBX, 0 ) );

    std::vector< std::shared_ptr< Instruction >> inst = { load, dump, load_from_stack };
    m. load( inst );
    m. run();

    THEN( "The value should be there and end up in register" ){
      REQUIRE( m. registers_state()[ RBX ] == 19 );
    }
  }
}
