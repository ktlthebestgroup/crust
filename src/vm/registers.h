#pragma once

#include <iostream>
#include <string>
#include <vector>

namespace registers {
  using REG_TYPE = uint16_t;
  using REG_VEC = std::vector< REG_TYPE >;

  enum Registers {
    RAX = 0,
    RBX,
    RCX,
    RDX,
    EIP,
    STP,
    BFP,
    CFL,
    REGN,
  };

  enum CompareFlags {
    ZERO = 1 << 0,
    POSV = 1 << 1,
    NEGV = 1 << 2,
  };

  bool check_registers_empty( const REG_VEC &x ){
    for( auto it : x ) if( it != 0 ) return false;
    return true;
  }

  void clear_registers( REG_VEC &x ){
    for( auto &val : x ) val = 0;
  }

  std::string register_to_string( const Registers &x ){
    switch( x ){
      case RAX:
        return "RAX";
      case RBX:
        return "RBX";
      case RCX:
        return "RCX";
      case RDX:
        return "RDX";
      case EIP:
        return "EIP";
      case STP:
        return "STP";
      case BFP:
        return "BFP";
      case CFL:
        return "CFL";
      case REGN:
        return "REGN";
      default:
        break;
    }

    return "UNKW";
  }

  std::ostream& operator<< ( std::ostream& out, Registers &x ){
    out << register_to_string( x );
    return out;
  }

  void print_registers( const REG_VEC &regs ){
    std::cout << "[\n";
    for( auto it : regs ){
      std::cout << "\t" << it << '\n';
    }
    std::cout << "]\n";
  }
}
