#pragma once

#include <vector>
#include <string>
#include <memory>
#include <iostream>
#include <assert.h>

#include "registers.h"
#include "instructions.h"

namespace vm {

  using namespace instructions;

  using REG_TYPE = uint16_t;
  using REG_VEC = std::vector< REG_TYPE >;

  class Machine {
    REG_VEC registers;
    std::vector< std::shared_ptr< Instruction >> inst;
    REG_VEC stack;
    size_t STACK_SIZE = 1 << 5;
    size_t action_count;

    bool strict;

    public:
    Machine( bool _strict = false ) : strict{ _strict } {
      registers. resize( REGN );
      stack. resize( STACK_SIZE );
    }

    void zero_registers(){
      clear_registers( registers );
    }

    void clear_stack(){
      stack. clear();
    }

    void add_inst( std::shared_ptr< Instruction > _inst ){
      inst. push_back( _inst );
    }

    void load( std::vector< std::shared_ptr< Instruction >> &_inst ){
      inst. clear();
      extend( _inst );
    }

    void extend( std::vector< std::shared_ptr< Instruction >> &_inst ){
      for( auto &it : _inst ) inst. push_back( it );
    }

    size_t get_action_count() const {
      return action_count;
    }

    void run(){
      action_count = 0;
      zero_registers();
      clear_stack();
      for( auto &eip = registers[ EIP ]; eip < inst. size(); ++ eip, ++ action_count ){ 
        auto it = inst[ eip ];
        it -> run( registers, stack );
      }

      if( strict ){
        assert( registers[ STP ] == 0 );
        assert( registers[ BFP ] < registers[ STP ] );
      }
    }

    REG_VEC registers_state() const {
      return registers;
    }

    size_t inst_size() const {
      return inst. size();
    }

    bool registers_empty() const {
      return check_registers_empty( registers );
    }

    std::ostream& print( std::ostream &out ){
      out << "Bytecode:\n";
      size_t line = 0;
      for( auto &it : inst ){
        out << "[" << line ++ << "]: ";
        it -> print( out );
      }
      out << '\n';
      out << "Bytecode size: " << inst.size();
      return out;
    }
  };

  std::ostream& operator<< ( std::ostream &out, Machine vm ){
    vm. print( out );
    return out;
  }

}
