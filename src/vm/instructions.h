#pragma once

#include <iostream>
#include <vector>

#include "registers.h"

namespace instructions {
  using namespace registers;

  using REG_TYPE = uint16_t;
  using REG_VEC = std::vector< REG_TYPE >;

  class Instruction {
    public:
    virtual void run( REG_VEC &regs, REG_VEC &stack ) = 0;
    virtual std::ostream& print( std::ostream &out ) = 0;
  };

  class Comment : public Instruction {
    std::string comment;

    public:
    Comment() = delete;
    Comment( const std::string &_comment ) : comment{ _comment } {}

    void run( REG_VEC &registers, REG_VEC &stack ){}

    std::ostream& print( std::ostream &out ) {
      out << "comment: " << comment << '\n';
      return out;
    }
  };

  class Add : public Instruction {
    Registers reg1, reg2;

    public:
    Add() = delete;
    Add( const Registers _r1, const Registers _r2 ) : reg1{ _r1 }, reg2{ _r2 } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ reg1 ] += registers[ reg2 ];
    }

    std::ostream& print( std::ostream &out ){
      out << "ADD " << reg1 << ' ' << reg2 << '\n';
      return out;
    }
  };

  class Sub : public Instruction {
    Registers reg1, reg2;

    public:
    Sub() = delete;
    Sub( const Registers _r1, const Registers _r2 ) : reg1{ _r1 }, reg2{ _r2 } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ reg1 ] -= registers[ reg2 ];
    }

    std::ostream& print( std::ostream &out ){
      out << "SUB " << reg1 << ' ' << reg2 << '\n';
      return out;
    }
  };

  class Mul : public Instruction {
    Registers reg1, reg2;

    public:
    Mul() = delete;
    Mul( const Registers _r1, const Registers _r2 ) : reg1{ _r1 }, reg2{ _r2 } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ reg1 ] *= registers[ reg2 ];
    }

    std::ostream& print( std::ostream &out ){
      out << "MUL " << reg1 << ' ' << reg2 << '\n';
      return out;
    }
  };

  class Div : public Instruction {
    Registers reg1, reg2;

    public:
    Div() = delete;
    Div( const Registers _r1, const Registers _r2 ) : reg1{ _r1 }, reg2{ _r2 } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ reg1 ] /= registers[ reg2 ];
    }

    std::ostream& print( std::ostream &out ){
      out << "DIV " << reg1 << ' ' << reg2 << '\n';
      return out;
    }
  };

  class Mod : public Instruction {
    Registers reg1, reg2;

    public:
    Mod() = delete;
    Mod( const Registers _r1, const Registers _r2 ) : reg1{ _r1 }, reg2{ _r2 } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ reg1 ] %= registers[ reg2 ];
    }

    std::ostream& print( std::ostream &out ){
      out << "MOD " << reg1 << ' ' << reg2 << '\n';
      return out;
    }
  };

  class And : public Instruction {
    Registers reg1, reg2;

    public:
    And() = delete;
    And( const Registers _r1, const Registers _r2 ) : reg1{ _r1 }, reg2{ _r2 } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ reg1 ] &= registers[ reg2 ];
    }

    std::ostream& print( std::ostream &out ){
      out << "AND " << reg1 << ' ' << reg2 << '\n';
      return out;
    }
  };

  class Or : public Instruction {
    Registers reg1, reg2;

    public:
    Or() = delete;
    Or( const Registers _r1, const Registers _r2 ) : reg1{ _r1 }, reg2{ _r2 } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ reg1 ] |= registers[ reg2 ];
    }

    std::ostream& print( std::ostream &out ){
      out << "OR " << reg1 << ' ' << reg2 << '\n';
      return out;
    }
  };

  class XOr : public Instruction {
    Registers reg1, reg2;

    public:
    XOr() = delete;
    XOr( const Registers _r1, const Registers _r2 ) : reg1{ _r1 }, reg2{ _r2 } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ reg1 ] ^= registers[ reg2 ];
    }

    std::ostream& print( std::ostream &out ){
      out << "XOR " << reg1 << ' ' << reg2 << '\n';
      return out;
    }
  };

  class LShift : public Instruction {
    Registers reg1, reg2;

    public:
    LShift() = delete;
    LShift( const Registers _r1, const Registers _r2 ) : reg1{ _r1 }, reg2{ _r2 } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ reg1 ] <<= registers[ reg2 ];
    }

    std::ostream& print( std::ostream &out ){
      out << "LSH " << reg1 << ' ' << reg2 << '\n';
      return out;
    }
  };

  class RShift : public Instruction {
    Registers reg1, reg2;

    public:
    RShift() = delete;
    RShift( const Registers _r1, const Registers _r2 ) : reg1{ _r1 }, reg2{ _r2 } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ reg1 ] >>= registers[ reg2 ];
    }

    std::ostream& print( std::ostream &out ){
      out << "RSH " << reg1 << ' ' << reg2 << '\n';
      return out;
    }
  };

  class Load : public Instruction {
    Registers reg;
    uint16_t val;

    public:
    Load() = delete;
    Load( const Registers _reg, const uint16_t _val ) : reg{ _reg }, val{ _val }{}

    uint16_t value() const {
      return val;
    }

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ reg ] = val;
    }

    std::ostream& print( std::ostream &out ){
      out << "LOAD " << reg << ' ' << val << '\n';
      return out;
    }
  };

  class Jump : public Instruction {
    uint16_t jump_to;

    public:
    Jump() = delete;
    Jump( const uint16_t val ) : jump_to{ val } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ EIP ] = jump_to - 1;
    }

    std::ostream& print( std::ostream &out ){
      out << "JMP " << jump_to << '\n';
      return out;
    }
  };

  class Push : public Instruction {
    Registers reg;

    public:
    Push() = delete;
    Push( const Registers _reg ) : reg{ _reg } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      stack[ registers[ STP ] ] = registers[ reg ];
      registers[ STP ] ++;
    }

    std::ostream& print( std::ostream &out ){
      out << "PUSH " << reg << '\n';
      return out;
    }
  };

  class Pop : public Instruction {
    Registers reg;

    public:
    Pop() = delete;
    Pop( const Registers _reg ) : reg{ _reg } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ STP ] --;
      registers[ reg ] = stack[ registers[ STP ] ];
    }

    std::ostream& print( std::ostream &out ){
      out << "POP " << reg << '\n';
      return out;
    }
  };

  class Drop : public Instruction {

    public:
    Drop() {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ STP ] --;
    }

    std::ostream& print( std::ostream &out ){
      out << "DROP\n";
      return out;
    }
  };

  class Compare : public Instruction {
    Registers reg1, reg2;

    public:
    Compare() = delete;
    Compare( const Registers _reg1, const Registers _reg2 ) : reg1{ _reg1 }, reg2{ _reg2 } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ CFL ] = 0;
      auto test_val = registers[ reg1 ] - registers[ reg2 ];
      if( (REG_TYPE) test_val == 0 ){
        registers[ CFL ] |= ZERO; 
      } else
      if( (REG_TYPE) test_val > registers[ reg1 ] ){
        registers[ CFL ] |= NEGV;
      } else {
        registers[ CFL ] |= POSV;
      }
    }

    std::ostream& print( std::ostream &out ){
      out << "CMP " << reg1 << ' ' << reg2 << '\n';
      return out;
    }
  };

  class LoadFromStack : public Instruction {
    Registers into;
    size_t from;

    public:
    LoadFromStack() = delete;
    LoadFromStack( const Registers _into, const size_t _from ) : into{ _into }, from{ _from } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      registers[ into ] = stack[ from ];
    }

    std::ostream& print( std::ostream &out ){
      out << "LOAD " << into << " [" << from << "]\n";
      return out;
    }
  };

  class DumpIntoStack : public Instruction {
    Registers from;
    size_t into;

    public:
    DumpIntoStack() = delete;
    DumpIntoStack( const Registers _from, const size_t _into ) : from{ _from }, into{ _into } {}

    void run( REG_VEC &registers, REG_VEC &stack ){
      stack[ into ] = registers[ from ];
    }

    std::ostream& print( std::ostream &out ){
      out << "LOAD " << into << " [" << from << "]\n";
      return out;
    }
  };

  class Call : public Instruction {
    size_t addr;

    public:
    Call() = delete;
    Call( const size_t _addr ) : addr{ _addr }{}

    void run( REG_VEC &registers, REG_VEC &stack ){
      Push( BFP ). run( registers, stack );
      Push( EIP ). run( registers, stack );
      registers[ BFP ] = registers[ STP ];
      registers[ EIP ] = addr - 1;
    }

    std::ostream& print( std::ostream &out ){
      out << "CALL " << addr << '\n';
      return out;
    }
  };

  class Return : public Instruction {
    public:
    Return(){}

    void run( REG_VEC &registers, REG_VEC &stack ){
      Pop( EIP ). run( registers, stack );
      Pop( BFP ). run( registers, stack );
    }

    std::ostream& print( std::ostream &out ){
      out << "RET\n";
      return out;
    }
  };

  class Nop : public Instruction {
    public:
    Nop(){}

    void run( REG_VEC &registers, REG_VEC &stack ){}

    std::ostream& print( std::ostream &out ){
      out << "NOP\n";
      return out;
    }
  };
}
