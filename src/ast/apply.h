#pragma once

#include "tree.h"
#include <iostream>

#define BINARY_APPLY(type)\
    class type : public base\
    {\
        tree left, right;\
    public:\
        virtual std::string get_type()\
        {\
            return #type;\
        }\
    };

#define UNARY_APPLY(type)\
    class type : public base\
    {\
        tree value;\
    public:\
        virtual std::string get_type()\
        {\
            return #type;\
        }\
    };

namespace ast
{
    BINARY_APPLY(add);
    BINARY_APPLY(sub);
    BINARY_APPLY(mul);
    BINARY_APPLY(div);
    UNARY_APPLY(negative);
    BINARY_APPLY(disj);
    BINARY_APPLY(conj);
    UNARY_APPLY(neg);
    BINARY_APPLY(eq);
    BINARY_APPLY(ne);
    BINARY_APPLY(gt);
    BINARY_APPLY(lt);
    BINARY_APPLY(ge);
    BINARY_APPLY(le);
    UNARY_APPLY(xpp);
    UNARY_APPLY(xmm);
    UNARY_APPLY(ppx);
    UNARY_APPLY(mmx);
    BINARY_APPLY(assign);
}