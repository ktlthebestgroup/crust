#pragma once

#include "tree.h"
#include <vector>

namespace ast
{
    class funcdef : public base
    {
    public:
        std::vector<ast::tree> commands;

        funcdef() : commands() {}
        funcdef(const std::vector<ast::tree>& commands) : commands(commands) {}

        virtual std::string get_type()
        {
            return "funcdef";
        }
    };
}