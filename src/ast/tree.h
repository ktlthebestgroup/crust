#pragma once

#include <string>
#include <memory>

namespace ast
{
    class base
    {
    public:
        virtual std::string get_type() = 0;
        virtual ~base() = default;
    };

    using tree = std::shared_ptr<base>;
}

#include "funcdef.h"