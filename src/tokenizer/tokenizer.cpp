
#include "tokenizer.h"
#include "../parsing/symbol.h"
#include "../../lib/lexing/algorithms.h"
#include "../../lib/lexing/minimization.h"

namespace tokenizer {

void tokenizer::add_bool_literals(){
  using namespace lexing;

  cls_pt -> insert( ( word( "true" ) | word( "false" ) ), sym_BOOL_LITERAL );
}

void tokenizer::add_keywords(){
  using namespace lexing;

  cls_pt -> insert( word( "auto" ), sym_AUTO );
  cls_pt -> insert( word( "bool" ), sym_BOOL );
  cls_pt -> insert( word( "char" ), sym_CHAR );
  cls_pt -> insert( word( "int" ), sym_INT );
  cls_pt -> insert( word( "float" ), sym_FLOAT );
  cls_pt -> insert( word( "double" ), sym_DOUBLE );
  cls_pt -> insert( word( "void" ), sym_VOID );
  cls_pt -> insert( word( "type" ), sym_TYPE );
  cls_pt -> insert( word( "typename" ), sym_TYPENAME );

  cls_pt -> insert( word( "mut" ), sym_MUT );

  cls_pt -> insert( word( "struct" ), sym_STRUCT );
  cls_pt -> insert( word( "clique" ), sym_CLIQUE );
  cls_pt -> insert( word( "template" ), sym_TEMPLATE );
  cls_pt -> insert( word( "enum" ), sym_ENUM );
  cls_pt -> insert( word( "trait" ), sym_TRAIT );
  //cls_pt -> insert( word( "public" ), sym_PUBLIC );
  //cls_pt -> insert( word( "private" ), sym_PRIVATE );
  cls_pt -> insert( word( "impl" ), sym_IMPL );

  cls_pt -> insert( word( "if" ), sym_IF );
  cls_pt -> insert( word( "else" ), sym_ELSE );
  cls_pt -> insert( word( "elif" ), sym_ELIF );
  cls_pt -> insert( word( "import" ), sym_IMPORT );
  cls_pt -> insert( word( "from" ), sym_FROM );
  cls_pt -> insert( word( "as" ), sym_AS );
  cls_pt -> insert( word( "for" ), sym_FOR );
  cls_pt -> insert( word( "break" ), sym_BREAK );
  cls_pt -> insert( word( "continue" ), sym_CONTINUE );
  cls_pt -> insert( word( "while" ), sym_WHILE );
  cls_pt -> insert( word( "loop" ), sym_LOOP );
  cls_pt -> insert( word( "foreach" ), sym_FOREACH );
  cls_pt -> insert( word( "return" ), sym_RETURN );
  cls_pt -> insert( word( "require" ), sym_REQUIRE );

  return;
}

void tokenizer::add_operators(){
  using namespace lexing;

  cls_pt -> insert( just( '+' ), sym_ADD );
  cls_pt -> insert( just( '-' ), sym_SUB );
  cls_pt -> insert( just( '*' ), sym_MUL );
  cls_pt -> insert( just( '/' ), sym_DIV );
  cls_pt -> insert( just( '%' ), sym_MOD );
  cls_pt -> insert( just( '&' ), sym_BITAND );
  cls_pt -> insert( just( '|' ), sym_BITOR );
  cls_pt -> insert( just( '^' ), sym_BITXOR );
  cls_pt -> insert( just( '!' ), sym_NOT );
  cls_pt -> insert( word( "<<" ), sym_LSHIFT );
  cls_pt -> insert( word( ">>" ), sym_RSHIFT );

  cls_pt -> insert( just( '=' ), sym_ASSIGN );
  cls_pt -> insert( word( "+=" ), sym_ADDASSIGN );
  cls_pt -> insert( word( "-=" ), sym_SUBASSIGN );
  cls_pt -> insert( word( "*=" ), sym_MULASSIGN );
  cls_pt -> insert( word( "/=" ), sym_DIVASSIGN );
  cls_pt -> insert( word( "%=" ), sym_MODASSIGN );
  cls_pt -> insert( word( "&=" ), sym_BITANDASSIGN );
  cls_pt -> insert( word( "|=" ), sym_BITORASSIGN );
  cls_pt -> insert( word( "^=" ), sym_BITXORASSIGN );
  cls_pt -> insert( word( "<<=" ), sym_LSHIFTASSIGN );
  cls_pt -> insert( word( ">>=" ), sym_RSHIFTASSIGN );

  cls_pt -> insert( word( "+>" ), sym_PLUSARROW );

  cls_pt -> insert( word( "==" ), sym_EQ );
  cls_pt -> insert( word( "!=" ), sym_NE );
  cls_pt -> insert( just( '<' ), sym_LT );
  cls_pt -> insert( just( '>' ), sym_GT );
  cls_pt -> insert( word( "<=" ), sym_LE );
  cls_pt -> insert( word( ">=" ), sym_GE );
}

void tokenizer::add_other_symbols(){
  using namespace lexing;

  cls_pt -> insert( just( '[' ), sym_LBRA );
  cls_pt -> insert( just( ']' ), sym_RBRA );
  cls_pt -> insert( just( '(' ), sym_LPAR );
  cls_pt -> insert( just( ')' ), sym_RPAR );
  cls_pt -> insert( just( '{' ), sym_LCUR );
  cls_pt -> insert( just( '}' ), sym_RCUR );
  cls_pt -> insert( just( ';' ), sym_SEMICOLON ); 
  cls_pt -> insert( just( ':' ), sym_COLON );
  cls_pt -> insert( just( '?' ), sym_QUESTION );
  cls_pt -> insert( just( '.' ), sym_DOT );
  cls_pt -> insert( just( ',' ), sym_COMMA );
  cls_pt -> insert( just( '$' ), sym_DOLLAR );
  cls_pt -> insert( just( '#' ), sym_HASHTAG );
  cls_pt -> insert( just( '@' ), sym_EMAIL );
  cls_pt -> insert( just( '~' ), sym_TILDA );
  cls_pt -> insert( just( '`' ), sym_BACKQUOTE );
  cls_pt -> insert( just( '\'' ), sym_QUOT );
  // cls. insert( just( '_' ), sym_UNDERSCORE );
}

void tokenizer::add_whitespace(){
  using namespace lexing;

  auto whitespace = ( just( ' ' ) | just( '\t' ) | just( '\n' ) | just( '\r' ) ).plus();
  cls_pt -> insert( whitespace, sym_WHITESPACE );
}

void tokenizer::add_binary_num(){
  using namespace lexing;

  auto prologue = word( "0b" );
  auto numeral_part = ( just( '0' ) | just( '1' ) ). plus();
  auto binary = prologue * numeral_part;
  cls_pt -> insert( binary, sym_BINARY_LITERAL );
}

void tokenizer::add_octal_num(){
  using namespace lexing;

  auto prologue = word( "0o" );
  auto numeral_part =  just( '0' ). plus() | ( range( '1', '7' ) * range( '0', '7' ).star() ). plus();
  auto octal = prologue * numeral_part;
  cls_pt -> insert( octal, sym_OCTAL_LITERAL );
}

void tokenizer::add_hex_num(){
  using namespace lexing;

  auto prologue = word( "0x" );
  auto chardigit = range( 'a', 'f' ) | range( 'A', 'F' );
  auto numeraldigit = range( '0', '9' );
  auto numeral_part = just( '0' ). plus() | ( ( range( '1', '9' ) | chardigit ) * ( numeraldigit | chardigit ). star() ). plus();
  auto hex = prologue * numeral_part;
  cls_pt -> insert( hex, sym_HEX_LITERAL );
}

void tokenizer::add_decimal_num(){
  using namespace lexing;

  auto digit = range( '0', '9' );
  auto decimal = just( '0' ) | ( range( '1', '9' ) * digit. star() ). plus();
  cls_pt -> insert( decimal, sym_DECIMAL_LITERAL );
}

void tokenizer::add_floating_num(){
  using namespace lexing;

  auto digit = range( '0', '9' );
  auto integer_part = just( '0' ) | ( range( '1', '9' ) * digit. star() ). plus();
  auto exponential_part = (just( 'e' ) | just( 'E' )) * just( '-' ). optional() * digit. plus();
  auto floatingpoint_part = just( '.' ) * digit. plus() * exponential_part. optional();
  auto floating = integer_part * ( exponential_part | floatingpoint_part );

  cls_pt -> insert( floating, sym_DOUBLE_LITERAL );
}

void tokenizer::add_identifiers(){
  using namespace lexing;

  auto identchar = range( 'a', 'z' ) | range( 'A', 'Z' ) | just( '_' );
  auto digit = range( '0', '9' );
  auto identifier = identchar * ( identchar | digit ). star();
  cls_pt -> insert( identifier, sym_IDENTIFIER );
}

void tokenizer::add_chars(){
  using namespace lexing;
  
  auto chrval = range( 'A', 'Z' ) | range( 'a', 'z' ) | range( '0', '9' ) | \
              ( just( '!' ) | just( '@' ) | just( '#' ) | just( '$' ) | \
                just( '%' ) | just( '^' ) | just( '&' ) | just( '*' ) | \
                just( '(' ) | just( ')' ) | just( '-' ) | just( '_' ) | \
                just( '=' ) | just( '+' ) | just( '/' ) | just( '|' ) | \
                just( ',' ) | just( '.' ) | just( '<' ) | just( '>' ) | \
                just( ';' ) | just( ':' ) | just( '[' ) | just( ']' ) | \
                just( '"' ) | just( '?' ) | just( '{' ) | just( '}' ) | \
                just( '`' ) | just( '~' ) | just( ' ' ) );
  auto backslash = just( '\\' );
  auto hexdigit = range( '0', '9' ) | range( 'a', 'f' ) | range( 'A', 'F' );
  auto special = just( '\\' ) | just( 'n' ) | just( 't' ) | just( 'r' ) | just( '\'' ) | \
                 ( just( 'x' ) * hexdigit * hexdigit );
  auto specialchar = backslash * special;
  auto chr = just( '\'' ) * ( chrval | specialchar ) * just( '\'' ) ;

  cls_pt -> insert( chr, sym_CHAR_LITERAL );
}

void tokenizer::add_strings(){
  using namespace lexing;

  auto chrval = range( 'A', 'Z' ) | range( 'a', 'z' ) | range( '0', '9' ) | \
              ( just( '!' ) | just( '@' ) | just( '#' ) | just( '$' ) | \
                just( '%' ) | just( '^' ) | just( '&' ) | just( '*' ) | \
                just( '(' ) | just( ')' ) | just( '-' ) | just( '_' ) | \
                just( '=' ) | just( '+' ) | just( '/' ) | just( '|' ) | \
                just( ',' ) | just( '.' ) | just( '<' ) | just( '>' ) | \
                just( ';' ) | just( ':' ) | just( '[' ) | just( ']' ) | \
                just( '?' ) | just( '{' ) | just( '}' ) | just( '\'' ) | \
                just( '`' ) | just( '~' ) | just( ' ' ) | just( '\n' ) | \
                just( '\t' ) );

  auto backslash = just( '\\' );
  auto hexdigit = range( '0', '9' ) | range( 'a', 'f' ) | range( 'A', 'F' );
  auto special = just( '\\' ) | just( 'n' ) | just( 't' ) | just( 'r' ) | just( '"' ) | \
                 ( just( 'x' ) * hexdigit * hexdigit );
  auto specialchar = backslash * special;
  auto str = just( '"' ) * ( chrval | specialchar ).star() * just( '"' ) ;

  cls_pt -> insert( str, sym_STRING_LITERAL );
}

void tokenizer::add_comments(){
  using namespace lexing;

  auto inline_comments = word( "//" ) * every<char>(). without( '\n' );
  auto multiline_comments = word( "/*" ) * every<char>() * word( "*/" );
  auto comments = multiline_comments | inline_comments;
  cls_pt -> insert( comments, sym_COMMENTS );
}

void tokenizer::buildclassifier(){
  cls_pt = std::unique_ptr<lexing::classifier< char, symboltype >>(new lexing::classifier< char, symboltype >( sym_ERROR ));

  add_bool_literals();
  add_decimal_num();
  add_binary_num();
  add_octal_num();
  add_hex_num();
  add_floating_num();
  add_identifiers();
  add_chars();
  add_strings();
  add_keywords();
  add_operators();
  add_other_symbols();
  add_whitespace();
  add_comments();

  return;
}

std::pair< symboltype, long unsigned int> tokenizer::skipwhitespace(){
  if( read. eof() ){
    return std::pair< symboltype, unsigned long >{ sym_EOF, 0 };
  }
  auto res = readandclassify( *cls_pt, read );
  while( res. first == sym_COMMENT || res. first == sym_WHITESPACE ){
    read. commit( res. second );
    if( read. eof() ){
      return std::pair< symboltype, unsigned long >{ sym_EOF, 0 };
    }
    res = readandclassify( *cls_pt, read );
  }
  return res;
}

void tokenizer::setcurrenttoken(){
  if( !current_empty ){
    return;
  }

  current_location = location( read. line, read. column );
  auto res = skipwhitespace();


  if( res. first == sym_DECIMAL_LITERAL || 
      res. first == sym_DOUBLE_LITERAL || 
      res. first == sym_BINARY_LITERAL || 
      res. first == sym_OCTAL_LITERAL || 
      res. first == sym_HEX_LITERAL ){
    auto some_val = parse_number_literal( res );
    current = some_val;
  } else 
  if( res. first == sym_BOOL_LITERAL ){
    current = parse_bool_literal( res );
  } else
  if( res. first == sym_CHAR_LITERAL ){
    current = parse_char_literal( res );
  } else
  if( res. first == sym_STRING_LITERAL ){
    current = parse_string_literal( res );
  } else
  if( res. first == sym_IDENTIFIER ){
    current = parse_identifier( res );
  } else {
    current = symbol( res. first, current_location );
  }
  this -> current_empty = false;
  read. commit( res. second );
}

symbol tokenizer::parse_number_literal( std::pair< symboltype, size_t > res ){
  if( res. first == sym_BINARY_LITERAL){
    return parse_binary_literal ( res );
  } else
  if( res. first == sym_OCTAL_LITERAL ){
    return parse_octal_literal  ( res );
  } else
  if( res. first == sym_HEX_LITERAL ){
    return parse_hex_literal    ( res );
  } else
  if( res. first == sym_DECIMAL_LITERAL ){
    return parse_decimal_literal( res );
  } else
  if( res. first == sym_DOUBLE_LITERAL ){
    //auto t = parse_double_literal ( res );
    //return t;

    return parse_double_literal( res );
  }
}

symbol tokenizer::parse_binary_literal( std::pair< symboltype, size_t > res ){
  int val = 0;
  for( size_t i = 2; i < res. second; ++ i ){ // skipping 0b
    val *= 2;
    val += (read. peek( i ) - '0');
  }
  return symbol( res.first, current_location, val );
}

symbol tokenizer::parse_octal_literal( std::pair< symboltype, size_t > res ){
  int val = 0;
  for( size_t i = 2; i < res. second; ++ i ){ // skipping 0o
    val *= 8;
    val += (read. peek( i ) - '0');
  }
  return symbol( res.first, current_location, val );
}

symbol tokenizer::parse_hex_literal( std::pair< symboltype, size_t > res ){
  std::string translate = "0123456789abcdef";
  int val = 0;
  for( size_t i = 2; i < res. second; ++ i ){ // skipping 0x
    val *= 16;
    char c = read. peek( i );
    if( 'A' <= c && c <= 'F' ) c = c - 'A' + 'a';
    val += translate.find( c );
  }
  return symbol( res.first, current_location, val );
}

symbol tokenizer::parse_decimal_literal( std::pair< symboltype, size_t > res ){
  int val = 0;
  for( size_t i = 0; i < res. second; ++ i ){
    val *= 10;
    val += (read. peek( i ) - '0');
  }
  return symbol( res.first, current_location, val );
}

symbol tokenizer::parse_double_literal( std::pair< symboltype, size_t > res ){
  std::string temp;
  for( size_t i = 0; i < res. second; ++ i ){
    temp += read. peek( i );
  }
  auto val = std::stod( temp );
  auto sym = symbol( res.first, current_location, val );
  return sym;
}

void tokenizer::commit(){ 
  if( current. type != sym_EOF )
    current_empty = true; 
}

symbol tokenizer::parse_bool_literal( std::pair< symboltype, size_t > res ){
  std::string val;
  for( size_t i = 0; i < res. second; ++ i ){
    val += read. peek( i );
  }
  if( val == "true" )
    return symbol( res. first, current_location, true );
  if( val == "false" )
    return symbol( res. first, current_location, false );
}

symbol tokenizer::parse_char_literal( std::pair< symboltype, size_t > res ){
  if( res. second == 3 ){
    return symbol( res. first, current_location, read. peek( 1 ) );
  }
  char val = 0;
  switch( read. peek( 2 ) ){
    case '\\': val = '\\'; break;
    case 'n':  val = '\n'; break;
    case 't':  val = '\t'; break;
    case 'r':  val = '\r'; break;
    case '\'': val = '\''; break;
    case 'x':  
      std::string translate = "0123456789abcdef";
      int a = translate.find( std::tolower( read. peek( 3 ) ) );
      int b = translate.find( std::tolower( read. peek( 4 ) ) );
      val = a * 16 + b;
      break;
  }

  return symbol( res. first, current_location, val );
}

symbol tokenizer::parse_string_literal( std::pair< symboltype, size_t > res ){
  std::string temp;
  for( size_t i = 1; i < res. second - 1; ++ i ){
    temp += read. peek( i );
  }

  std::string res_str;
  for( size_t i = 0; i < res. second - 2; ++ i ){
    if( temp[ i ] == '\\' ){
      i += 1;
      char val = 0;
      switch( temp[ i ] ){
        case '\\': val = '\\'; break;
        case 'n':  val = '\n'; break;
        case 't':  val = '\t'; break;
        case 'r':  val = '\r'; break;
        case '"': val = '"'; break;
        case 'x':  
          std::string translate = "0123456789abcdef";
          int a = translate.find( std::tolower( read. peek( 3 ) ) );
          int b = translate.find( std::tolower( read. peek( 4 ) ) );
          val = a * 16 + b;
          break;
      }

      res_str += val;
    } else {
      res_str += temp[ i ];
    }
  }

  return symbol( res. first, current_location, res_str );
}

symbol tokenizer::parse_identifier( std::pair< symboltype, size_t > res ){
  std::string id;
  for( size_t i = 0; i < res. second; ++ i ){
    id += read. peek( i );
  }
  return symbol( res. first, current_location, id );
}

bool tokenizer::hastoken(){ return !current_empty; }

bool tokenizer::canread(){ return current.type != sym_EOF; }

symbol tokenizer::gettoken(){
  if( !hastoken() ){
    setcurrenttoken();
  }
  return current;
}

} // namespace 
