#pragma once

#include <memory>

#include "../../lib/filereader.h"
#include "../../lib/lexing/classifier.h"

#include "../parsing/location.h"
#include "../parsing/symbol.h"

namespace tokenizer {

class tokenizer {
  filereader read;

  symbol current = symbol( sym_EMPTY_TOKEN, {} );
  bool current_empty;
  location current_location;

  std::unique_ptr<lexing::classifier< char, symboltype >> cls_pt;

  void buildclassifier();

  void add_bool_literals();
  void add_keywords     ();
  void add_operators    ();
  void add_other_symbols();
  void add_whitespace   ();
  void add_binary_num   ();
  void add_octal_num    ();
  void add_hex_num      ();
  void add_decimal_num  ();
  void add_floating_num ();
  void add_identifiers  ();
  void add_chars        ();
  void add_strings      ();
  void add_comments     ();

  std::pair< symboltype, long unsigned int > skipwhitespace();

  symbol parse_number_literal ( std::pair< symboltype, size_t >);
  symbol parse_binary_literal ( std::pair< symboltype, size_t >);
  symbol parse_octal_literal  ( std::pair< symboltype, size_t >);
  symbol parse_hex_literal    ( std::pair< symboltype, size_t >);
  symbol parse_decimal_literal( std::pair< symboltype, size_t >);
  symbol parse_double_literal ( std::pair< symboltype, size_t >);
  symbol parse_bool_literal   ( std::pair< symboltype, size_t >);
  symbol parse_char_literal   ( std::pair< symboltype, size_t >);
  symbol parse_string_literal ( std::pair< symboltype, size_t >);
  symbol parse_identifier     ( std::pair< symboltype, size_t >);

  void setcurrenttoken();
  
  public:
  tokenizer( ) : read{ filereader() }, current_empty{ true } {
    buildclassifier();
  }

  tokenizer( filereader&& reader ) : read{ std::move(reader) }, current_empty{ true } {
    buildclassifier();
  }

  symbol gettoken();
  void commit();
  bool hastoken();
  bool canread();
};

} // namespace
