#include <iostream>
#include <string>
#include <fstream>

#include "tokenizer/tokenizer.h"
#include "../lib/filereader.h"

std::string file_to_read = "";

namespace {

void parse_args(int argc, char* argv[]){
  if( argc == 1 ){
    std::cout << "No files are provided! Aborting!\n";
    exit(1);
  }
  if( argc > 1 ){
    file_to_read = std::string(argv[1]);
  }
}

filereader get_filereader_from_filename( std::string filename ){
  std::ifstream *inputfile = new std::ifstream( filename );
  filereader inp( inputfile, filename );
  return inp;
}

} // main namespace

int main(int argc, char* argv[]){
  parse_args(argc, argv);
  filereader freader(std::move(get_filereader_from_filename( file_to_read )));
  tokenizer::tokenizer tok( std::move(freader) );
  while( tok. canread() ){
    auto sym = tok.gettoken();
    std::cout << sym << '\n';
    tok.commit();
  }
  return 0;
}
