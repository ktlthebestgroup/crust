#pragma once

#include "symbol.h"

class tokenizer
{
public:
    virtual symbol get() = 0;
};