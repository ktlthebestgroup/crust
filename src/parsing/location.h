#pragma once

#include <iostream>

struct location
{
    unsigned long line;
    unsigned long col;

    location() = default;
    location( unsigned long line, unsigned long col ) : line{ line }, col{ col } {}
};

std::ostream& operator<<(std::ostream& out, const location& info);
