#include "location.h"

#include <string>

std::ostream& operator<<(std::ostream& out, const location& info)
{
    out << info.line << '/' << info.col;
    return out;
}
