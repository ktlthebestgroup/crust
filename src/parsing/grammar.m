%startsymbol Program EOF

%infotype{ location } 
%symbol EMPTY_TOKEN
%symbol EOF
%symbol Program
%symbol COMMENT WHITESPACE
%symbol{ std::string } IDENTIFIER
%symbol BEGIN END

%symbol{ bool }        BOOLCONST
%symbol{ char }        CHARCONST
%symbol{ int }         INTEGERCONST
%symbol{ double }      DOUBLECONST
%symbol{ std::string } STRINGCONST

%symbol AUTO BOOL CHAR INT FLOAT DOUBLE VOID MUT CONST STRUCT CLIQUE TEMPLATE ENUM TRAIT TYPE TYPENAME PUBLIC PRIVATE IMPL
%symbol IF ELSE ELIF FOR BREAK WHILE CONTINUE LOOP FOREACH REQUIRE RETURN
%symbol IMPORT FROM AS 

%symbol ADD SUB MUL DIV MOD BITAND BITOR BITXOR NOT LSHIFT RSHIFT
%symbol ASSIGN ADDASSIGN SUBASSIGN MULASSIGN DIVASSIGN MODASSIGN BITANDASSIGN BITORASSIGN BITXORASSIGN LSHIFTASSIGN RSHIFTASSIGN
%symbol PLUSARROW
%symbol EQ NE LT GT LE GE

%symbol LBRA RBRA LPAR RPAR LCUR RCUR SEMICOLON COLON QUESTION DOT COMMA DOLLAR HASHTAG EMAIL TILDA BACKQUOTE QUOT

%symbol { bool }        BOOL_LITERAL
%symbol { int }         BINARY_LITERAL OCTAL_LITERAL HEX_LITERAL DECIMAL_LITERAL DOUBLE_LITERAL
%symbol { char }        CHAR_LITERAL 
%symbol { std::string } STRING_LITERAL
%symbol COMMENTS

%symbol PRINT FAIL
%symbol OR AND 
%symbol STAR
%symbol ARROW
%symbol LEFTSHIFT RIGHTSHIFT
%symbol PLUSPLUS MINUSMINUS
%symbol STRUCTDEF FUNCTION CONSTANT

%symbol ERROR

%symbol Expr Expr2 Expr3 Expr4 Expr5 Expr6 Expr7 Expr8 Expr9 Expr10 Expr11 Expr12
%symbol ExprList ExprListNE

%parsercode_h { #include "tokenizer.h" }
%symbolcode_h { #include "location.h" }
%symbolcode_h { #include "../ast/tree.h" }
%symbolcode_h { #include <vector> }
%symbolcode_h { #include <memory> }

%parameter { std::unique_ptr<tokenizer> } tok

%source { tok->get(); }

%rules

Program => ExprListNE;

Expr => Expr2 ASSIGN Expr
    | Expr2
    ;

Expr2 => Expr3 QUESTION Expr3 COLON Expr3
    | Expr3
    ;

Expr3 => Expr3 OR Expr4
    | Expr4
    ;

Expr4 => Expr4 AND Expr5
    | Expr5
    ;

Expr5 => NOT Expr5
    | Expr6
    ;

Expr6 => Expr7 EQ Expr7
    | Expr7 NE Expr7
    | Expr7 LT Expr7
    | Expr7 GT Expr7
    | Expr7 LE Expr7
    | Expr7 GE Expr7
    | Expr7
    ;
    
Expr7 => Expr7 ADD Expr8
    | Expr7 SUB Expr8
    | Expr8
    ;

Expr8 => Expr8 MUL Expr9
    | Expr8 DIV Expr9
    | Expr8 MOD Expr9
    | Expr9
    ;

Expr9 => SUB Expr9
    | Expr10
    ;

Expr10 => Expr11;

Expr11 => PLUSPLUS Expr12
    | MINUSMINUS Expr12
    | Expr12
    ;

Expr12 => Expr12 PLUSPLUS
    | Expr12 MINUSMINUS
    | Expr12 DOT IDENTIFIER
    | Expr12 LBRA Expr RBRA
    | IDENTIFIER
    | IDENTIFIER LPAR ExprList RPAR
    | BOOLCONST
    | INTEGERCONST
    | CHARCONST
    | DOUBLECONST
    | LPAR Expr RPAR
    ;

ExprList => ExprListNE
    |
    ;

ExprListNE => Expr
    | ExprListNE COMMA Expr
    ;
