#ifndef TEST_PARSING
#define TEST_PARSING

#include <parsing/symbol.h>
#include <parsing/parser.h>
#include <parsing/tokenizer.h>
#include <assert.h>
#include <vector>

#define sym(s) (symbol(sym##_##s, {}))
#define sym_p(s, ...) (symbol(sym##_##s, {},##__VA_ARGS__))

class TestTokenizer : public tokenizer
{
    std::vector<symbol> sequence;
public:
    TestTokenizer(std::initializer_list<symbol> init) : sequence(init) {}

    symbol get()
    {
        static size_t i = 0;
        if (i >= sequence.size())
        {
            throw std::out_of_range("no more symbols in sequence");
        }
        ++i;
        std::cout << sequence[i - 1] << '\n';
        return sequence[i - 1];
    }
};

void test_parsing()
{
    auto tok = std::unique_ptr<tokenizer>(new TestTokenizer({sym_p(INTEGERCONST, 5), sym(ADD), sym_p(INTEGERCONST, 16), sym(EOF)}));
    parser p(tok);
    p.debug = 1;
    auto s = p.parse(sym_Program);
    std::cout << s;
}

#endif