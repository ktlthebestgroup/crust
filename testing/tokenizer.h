#include <tokenizer/tokenizer.h>

#include <catch.hpp>
#include <sstream>
#include <vector>

#include <filereader.h>
#include <parsing/symbol.h>

std::vector< symbol > get_tokens( tokenizer::tokenizer &tok ){
  std::vector< symbol > res; 
  while( tok. canread() ){
    res. push_back( tok. gettoken() );
    tok. commit();
  }
  return res;
}

TEST_CASE( "Test simple tokens", "[tokenizer]" ){
  GIVEN( "Empty tokenizer" ){
    std::stringstream *ss = new std::stringstream();
    filereader freader( std::move(ss), "stringstream" );
    tokenizer::tokenizer tok( std::move(freader) );
    
    WHEN( "Given math ops" ){
      (*ss) << "+ - * / %";
      THEN( "They are parsed" ){
        auto tokens = get_tokens( tok );
        REQUIRE( tokens. size() == 6 );
        REQUIRE( tokens[ 0 ]. type == sym_ADD );
        REQUIRE( tokens[ 1 ]. type == sym_SUB );
        REQUIRE( tokens[ 2 ]. type == sym_MUL );
        REQUIRE( tokens[ 3 ]. type == sym_DIV );
        REQUIRE( tokens[ 4 ]. type == sym_MOD );
        REQUIRE( tokens[ 5 ]. type == sym_EOF );
      }
    }

    delete ss;
  }
}

TEST_CASE( "Test keywords", "[tokenizer]" ){
  GIVEN( "Empty tokenizer" ){
    std::stringstream *ss = new std::stringstream();
    filereader freader( std::move(ss), "stringstream" );
    tokenizer::tokenizer tok( std::move(freader) );
    
    WHEN( "Given keywords" ){
      (*ss) << 
        "auto \n"
        "bool \n"
        "char \n"
        "int \n"
        "float \n"
        "double \n"
        "void \n"
        "type \n"
        "typename \n"
        "mut \n"
        "struct \n"
        "clique \n"
        "template \n"
        "enum \n"
        "trait \n"
        "impl \n"
        "if \n"
        "else \n"
        "elif \n"
        "import \n"
        "from \n"
        "as \n"
        "for \n"
        "break \n"
        "continue \n"
        "while \n"
        "loop \n"
        "foreach \n"
        "return \n"
        "require \n";
      THEN( "They are parsed" ){
        auto tokens = get_tokens( tok );
        REQUIRE( tokens. size() == 31 );
        REQUIRE( tokens[  0 ]. type == sym_AUTO );
        REQUIRE( tokens[  1 ]. type == sym_BOOL );
        REQUIRE( tokens[  2 ]. type == sym_CHAR );
        REQUIRE( tokens[  3 ]. type == sym_INT );
        REQUIRE( tokens[  4 ]. type == sym_FLOAT );
        REQUIRE( tokens[  5 ]. type == sym_DOUBLE );
        REQUIRE( tokens[  6 ]. type == sym_VOID );
        REQUIRE( tokens[  7 ]. type == sym_TYPE );
        REQUIRE( tokens[  8 ]. type == sym_TYPENAME );
        REQUIRE( tokens[  9 ]. type == sym_MUT );
        REQUIRE( tokens[ 10 ]. type == sym_STRUCT );
        REQUIRE( tokens[ 11 ]. type == sym_CLIQUE );
        REQUIRE( tokens[ 12 ]. type == sym_TEMPLATE );
        REQUIRE( tokens[ 13 ]. type == sym_ENUM );
        REQUIRE( tokens[ 14 ]. type == sym_TRAIT );
        REQUIRE( tokens[ 15 ]. type == sym_IMPL );
        REQUIRE( tokens[ 16 ]. type == sym_IF );
        REQUIRE( tokens[ 17 ]. type == sym_ELSE );
        REQUIRE( tokens[ 18 ]. type == sym_ELIF );
        REQUIRE( tokens[ 19 ]. type == sym_IMPORT );
        REQUIRE( tokens[ 20 ]. type == sym_FROM );
        REQUIRE( tokens[ 21 ]. type == sym_AS );
        REQUIRE( tokens[ 22 ]. type == sym_FOR );
        REQUIRE( tokens[ 23 ]. type == sym_BREAK );
        REQUIRE( tokens[ 24 ]. type == sym_CONTINUE );
        REQUIRE( tokens[ 25 ]. type == sym_WHILE );
        REQUIRE( tokens[ 26 ]. type == sym_LOOP );
        REQUIRE( tokens[ 27 ]. type == sym_FOREACH );
        REQUIRE( tokens[ 28 ]. type == sym_RETURN );
        REQUIRE( tokens[ 29 ]. type == sym_REQUIRE );
        REQUIRE( tokens[ 30 ]. type == sym_EOF );
      }
    }

    delete ss;
  }
}

TEST_CASE( "Test number literals", "[tokenizer]" ){
  GIVEN( "Empty tokenizer" ){
    std::stringstream *ss = new std::stringstream();
    filereader freader( std::move(ss), "stringstream" );
    tokenizer::tokenizer tok( std::move(freader) );

    WHEN( "Given binary literals" ){
      (*ss) << "0b0000 0b00000000 0b1 0b10 0b11111111 0b100101010 0b1111111111111111111111111111111111111111111111111111111111111111";
      THEN( "It parses into decimal literals" ){
        auto tokens = get_tokens( tok );
        REQUIRE( tokens. size() == 8 );
        REQUIRE( ( tokens[0]. type == sym_BINARY_LITERAL && tokens[0]. get< int >() == 0 ) );
        REQUIRE( ( tokens[0]. type == sym_BINARY_LITERAL && tokens[1]. get< int >() == 0 ) );
        REQUIRE( ( tokens[0]. type == sym_BINARY_LITERAL && tokens[2]. get< int >() == 1 ) );
        REQUIRE( ( tokens[0]. type == sym_BINARY_LITERAL && tokens[3]. get< int >() == 2 ) );
        REQUIRE( ( tokens[0]. type == sym_BINARY_LITERAL && tokens[4]. get< int >() == 255 ) );
        REQUIRE( ( tokens[0]. type == sym_BINARY_LITERAL && tokens[5]. get< int >() == 298 ) );
        REQUIRE( ( tokens[0]. type == sym_BINARY_LITERAL && tokens[6]. get< int >() == -1 ) );
        REQUIRE( tokens[7]. type == sym_EOF );
      }
    }

    WHEN( "Given octal literals" ){
      (*ss) << "0o0000 0o00000000 0o1 0o10 0o11111111 0o100101010";
      THEN( "It parses into decimal literals" ){
        auto tokens = get_tokens( tok );
        REQUIRE( tokens. size() == 7 );
        REQUIRE( tokens[0]. type == sym_OCTAL_LITERAL ); REQUIRE( tokens[0]. get< int >() == 0 );
        REQUIRE( tokens[1]. type == sym_OCTAL_LITERAL ); REQUIRE( tokens[1]. get< int >() == 0 );
        REQUIRE( tokens[2]. type == sym_OCTAL_LITERAL ); REQUIRE( tokens[2]. get< int >() == 1 );
        REQUIRE( tokens[3]. type == sym_OCTAL_LITERAL ); REQUIRE( tokens[3]. get< int >() == 8 );
        REQUIRE( tokens[4]. type == sym_OCTAL_LITERAL ); REQUIRE( tokens[4]. get< int >() == 2396745 );
        REQUIRE( tokens[5]. type == sym_OCTAL_LITERAL ); REQUIRE( tokens[5]. get< int >() == 16810504 );
        REQUIRE( tokens[6]. type == sym_EOF );
      }
    }

    WHEN( "Given hex literals" ){
      (*ss) << "0x0 0x0000 0x1 0x10 0xdeadbeef 0xbabe";
      THEN( "It parses into decmial literals" ){
        auto tokens = get_tokens( tok );
        REQUIRE( tokens. size() == 7 );
        REQUIRE( tokens[0]. type == sym_HEX_LITERAL ); REQUIRE( tokens[0]. get< int >() == 0 );
        REQUIRE( tokens[1]. type == sym_HEX_LITERAL ); REQUIRE( tokens[1]. get< int >() == 0 );
        REQUIRE( tokens[2]. type == sym_HEX_LITERAL ); REQUIRE( tokens[2]. get< int >() == 1 );
        REQUIRE( tokens[3]. type == sym_HEX_LITERAL ); REQUIRE( tokens[3]. get< int >() == 16 );
        REQUIRE( tokens[4]. type == sym_HEX_LITERAL ); REQUIRE( tokens[4]. get< int >() == int(3735928559) );
        REQUIRE( tokens[5]. type == sym_HEX_LITERAL ); REQUIRE( tokens[5]. get< int >() == 47806 );
        REQUIRE( tokens[6]. type == sym_EOF );
      }
    }

    WHEN( "Given decimal literals" ){
      (*ss) << "0 1 2 3 10 12321 15871747";
      THEN( "It parses into decimal literals" ){
        auto tokens = get_tokens( tok );
        REQUIRE( tokens. size() == 8 );
        REQUIRE( tokens[0]. type == sym_DECIMAL_LITERAL ); REQUIRE( tokens[0]. get< int >() == 0 );
        REQUIRE( tokens[1]. type == sym_DECIMAL_LITERAL ); REQUIRE( tokens[1]. get< int >() == 1 );
        REQUIRE( tokens[2]. type == sym_DECIMAL_LITERAL ); REQUIRE( tokens[2]. get< int >() == 2 );
        REQUIRE( tokens[3]. type == sym_DECIMAL_LITERAL ); REQUIRE( tokens[3]. get< int >() == 3 );
        REQUIRE( tokens[4]. type == sym_DECIMAL_LITERAL ); REQUIRE( tokens[4]. get< int >() == 10 );
        REQUIRE( tokens[5]. type == sym_DECIMAL_LITERAL ); REQUIRE( tokens[5]. get< int >() == 12321 );
        REQUIRE( tokens[6]. type == sym_DECIMAL_LITERAL ); REQUIRE( tokens[6]. get< int >() == 15871747 );
        REQUIRE( tokens[7]. type == sym_EOF );
      }
    }

    WHEN( "Given floating literals" ){
      (*ss) << "0.0 0.0000 1.0 1e2 1e5 1.123 1e-1 1.123e-3";
      THEN( "It parses into floating literals" ){
        auto tokens = get_tokens( tok );
        REQUIRE( tokens. size() == 9 );
        REQUIRE( tokens[0]. type == sym_DOUBLE_LITERAL ); REQUIRE( tokens[0]. get< double >() == Approx(0.0) );
        REQUIRE( tokens[1]. type == sym_DOUBLE_LITERAL ); REQUIRE( tokens[1]. get< double >() == Approx(0.0) );
        REQUIRE( tokens[2]. type == sym_DOUBLE_LITERAL ); REQUIRE( tokens[2]. get< double >() == Approx(1.0) );
        REQUIRE( tokens[3]. type == sym_DOUBLE_LITERAL ); REQUIRE( tokens[3]. get< double >() == Approx(100) );
        REQUIRE( tokens[4]. type == sym_DOUBLE_LITERAL ); REQUIRE( tokens[4]. get< double >() == Approx(100000) );
        REQUIRE( tokens[5]. type == sym_DOUBLE_LITERAL ); REQUIRE( tokens[5]. get< double >() == Approx(1.123) );
        REQUIRE( tokens[6]. type == sym_DOUBLE_LITERAL ); REQUIRE( tokens[6]. get< double >() == Approx(0.1) );
        REQUIRE( tokens[7]. type == sym_DOUBLE_LITERAL ); REQUIRE( tokens[7]. get< double >() == Approx(0.001123) );
        REQUIRE( tokens[8]. type == sym_EOF );
      }
    }

    delete ss;
  }
}

TEST_CASE( "Test identifiers", "[tokenizer]" ){
  GIVEN( "Empty tokenizer" ){
    std::stringstream *ss = new std::stringstream();
    filereader freader( std::move(ss), "stringstream" );
    tokenizer::tokenizer tok( std::move(freader) );

    WHEN( "Given identifiers" ){
      (*ss) << "danel Olzhas Tatyana";
      THEN( "It parses identifiers" ){
        auto tokens = get_tokens( tok );
        REQUIRE( tokens. size() == 4 );
        REQUIRE( tokens[0]. type == sym_IDENTIFIER ); REQUIRE( tokens[0]. get< std::string >() == "danel" );
        REQUIRE( tokens[1]. type == sym_IDENTIFIER ); REQUIRE( tokens[1]. get< std::string >() == "Olzhas" );
        REQUIRE( tokens[2]. type == sym_IDENTIFIER ); REQUIRE( tokens[2]. get< std::string >() == "Tatyana" );
        REQUIRE( tokens[3]. type == sym_EOF );
      }
    }

    WHEN( "Given identifiers" ){
      (*ss) << "____ _00101 1_ab danel'olzhas";
      THEN( "It parses identifiers" ){
        auto tokens = get_tokens( tok );
        REQUIRE( tokens. size() == 8 );
        REQUIRE( tokens[0]. type == sym_IDENTIFIER );      REQUIRE( tokens[0]. get< std::string >() == "____" );
        REQUIRE( tokens[1]. type == sym_IDENTIFIER );      REQUIRE( tokens[1]. get< std::string >() == "_00101" );
        REQUIRE( tokens[2]. type == sym_DECIMAL_LITERAL ); REQUIRE( tokens[2]. get< int >() == 1 );
        REQUIRE( tokens[3]. type == sym_IDENTIFIER );      REQUIRE( tokens[3]. get< std::string >() == "_ab" );
        REQUIRE( tokens[4]. type == sym_IDENTIFIER );      REQUIRE( tokens[4]. get< std::string >() == "danel" );
        REQUIRE( tokens[5]. type == sym_QUOT );      
        REQUIRE( tokens[6]. type == sym_IDENTIFIER );      REQUIRE( tokens[6]. get< std::string >() == "olzhas" );
        REQUIRE( tokens[7]. type == sym_EOF );
      }
    }

    delete ss;
  }
}

TEST_CASE( "Test char and string literals", "[tokenizer]" ){
  GIVEN( "Empty tokenizer" ){
    std::stringstream *ss = new std::stringstream();
    filereader freader( std::move(ss), "stringstream" );
    tokenizer::tokenizer tok( std::move(freader) );
    
    WHEN( "Given char literals" ){
      (*ss) << "'C' 'r' 'U' 's' 't' '\\t' '\\n' '\\xff' '\\\\'";
      THEN( "It parses char literals" ){
        auto tokens = get_tokens( tok );
        REQUIRE( tokens. size() == 10 );
        REQUIRE( tokens[0]. type == sym_CHAR_LITERAL ); REQUIRE( tokens[0]. get< char >() == 'C' );
        REQUIRE( tokens[1]. type == sym_CHAR_LITERAL ); REQUIRE( tokens[1]. get< char >() == 'r' );
        REQUIRE( tokens[2]. type == sym_CHAR_LITERAL ); REQUIRE( tokens[2]. get< char >() == 'U' );
        REQUIRE( tokens[3]. type == sym_CHAR_LITERAL ); REQUIRE( tokens[3]. get< char >() == 's' );
        REQUIRE( tokens[4]. type == sym_CHAR_LITERAL ); REQUIRE( tokens[4]. get< char >() == 't' );
        REQUIRE( tokens[5]. type == sym_CHAR_LITERAL ); REQUIRE( tokens[5]. get< char >() == '\t' );
        REQUIRE( tokens[6]. type == sym_CHAR_LITERAL ); REQUIRE( tokens[6]. get< char >() == '\n' );
        REQUIRE( tokens[7]. type == sym_CHAR_LITERAL ); REQUIRE( tokens[7]. get< char >() == char(255) );
        REQUIRE( tokens[8]. type == sym_CHAR_LITERAL ); REQUIRE( tokens[8]. get< char >() == '\\' );
        REQUIRE( tokens[9]. type == sym_EOF );
      }
    }

    WHEN( "Given string literals" ){
      (*ss) << 
        "\"Hello World!\""
        "\"123456789\""
        "\"Kek\nKek\""
        "\"Kek\tKek\""
        "\"Kek\\nKek\""
        "\"Kek\\\\Kek\""
        "\"'Kek'\""
        ;
      THEN( "It parses char literals" ){
        auto tokens = get_tokens( tok );
        REQUIRE( tokens. size() == 8 );
        REQUIRE( tokens[0]. type == sym_STRING_LITERAL ); REQUIRE( tokens[0]. get< std::string >() == "Hello World!" );
        REQUIRE( tokens[1]. type == sym_STRING_LITERAL ); REQUIRE( tokens[1]. get< std::string >() == "123456789" );
        REQUIRE( tokens[2]. type == sym_STRING_LITERAL ); REQUIRE( tokens[2]. get< std::string >() == "Kek\nKek" );
        REQUIRE( tokens[3]. type == sym_STRING_LITERAL ); REQUIRE( tokens[3]. get< std::string >() == "Kek\tKek" );
        REQUIRE( tokens[4]. type == sym_STRING_LITERAL ); REQUIRE( tokens[4]. get< std::string >() == "Kek\nKek" );
        REQUIRE( tokens[5]. type == sym_STRING_LITERAL ); REQUIRE( tokens[5]. get< std::string >() == "Kek\\Kek" );
        REQUIRE( tokens[6]. type == sym_STRING_LITERAL ); REQUIRE( tokens[6]. get< std::string >() == "'Kek'" );
        REQUIRE( tokens[7]. type == sym_EOF );
      }
    }

    delete ss;
  }
}
