Maph = lib/maphoon2021c
Flags = -Wreturn-type -pedantic -pedantic-errors -Wundef -std=c++17 -I lib -I src
CPP = g++
C = $(CPP) $(Flags) -c
CL = $(CPP) $(Flags)

ParsingO = bin/parsing/location.o bin/parsing/symbol.o bin/parsing/parser.o

run: bin/crust
	./bin/crust example

bin/crust: bin/parsing/symbol.o bin/tokenizer/tokenizer.o bin/lib/filereader.o bin/parsing/location.o bin/crust.o
	$(CL) bin/crust.o bin/tokenizer/tokenizer.o bin/lib/filereader.o bin/parsing/symbol.o bin/parsing/location.o -o bin/crust

bin/crust.o: src/crust.cpp
	$(C) src/crust.cpp -o bin/crust.o

bin/test_parsing : testing/test.cpp testing/parsing.h $(ParsingO)
	$(CL) testing/test.cpp $(ParsingO) -o bin/test_parsing -D PARSING

# Dependencies
bin/lib/filereader.o: lib/filereader.h lib/filereader.cpp
	$(C) lib/filereader.cpp -o bin/lib/filereader.o

# Tokenizer
bin/tokenizer/tokenizer.o: src/tokenizer/tokenizer.h src/tokenizer/tokenizer.cpp src/parsing/symbol.h lib/filereader.h
	$(C) src/tokenizer/tokenizer.cpp -o bin/tokenizer/tokenizer.o

# Parser
AstH = src/ast/tree.h src/ast/funcdef.h src/ast/apply.h
LexingO = $(Maph)/filereader.o $(Maph)/lexing/partition.o $(Maph)/lexing/stateset.o

bin/parsing/parser.o : src/parsing/parser.cpp src/parsing/parser.h src/ast/tree.h $(AstH)
	$(C) src/parsing/parser.cpp -o bin/parsing/parser.o

bin/parsing/symbol.o : src/parsing/symbol.cpp src/parsing/symbol.h src/parsing/location.h
	$(C) src/parsing/symbol.cpp -o bin/parsing/symbol.o

bin/parsing/location.o : src/parsing/location.h src/parsing/location.cpp
	$(C) src/parsing/location.cpp -o bin/parsing/location.o

# Files generated by Maphoon
src/parsing/symbol.cpp : src/parsing/grammar.m
	$(Maph)/maphoon src/parsing/grammar.m src/parsing/ $(Maph)

src/parsing/symbol.h : src/parsing/grammar.m
	$(Maph)/maphoon src/parsing/grammar.m src/parsing/ $(Maph)

src/parsing/parser.cpp : src/parsing/grammar.m
	$(Maph)/maphoon src/parsing/grammar.m src/parsing/ $(Maph)

src/parsing/parser.h : src/parsing/grammar.m
	$(Maph)/maphoon src/parsing/grammar.m src/parsing/ $(Maph)

test: bin/testing/test_main
	./bin/testing/test_main

bin/testing/test_main: bin/parsing/symbol.o bin/parsing/location.o bin/lib/filereader.o bin/tokenizer/tokenizer.o bin/testing/test_main.o testing/tokenizer.h   
	$(CL) bin/tokenizer/tokenizer.o bin/testing/test_main.o bin/lib/filereader.o bin/parsing/symbol.o bin/parsing/location.o -o bin/testing/test_main

bin/testing/test_main.o: testing/test.cpp testing/tokenizer.h
	$(C) testing/test.cpp -o bin/testing/test_main.o

clean:
	rm -rf bin
	rm -rf src/parsing/symbol*
	rm -rf src/parsing/parser*

DIRS=bin bin/parsing bin/tokenizer bin/lib bin/testing
$(shell mkdir -p $(DIRS))
