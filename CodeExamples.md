# Examples of Crust
## Variable definition
```
int i; // error
auto a = 1 + 1; // int
mut int i = 0;
char c = 'a';
float f = 3.14;
bool b = true;
String s = "foobar";

int& p = &i; // error
mut int& p = &i; // error
(mut int)& p = &i; //ok
i = *p;

int a2 = 5;
int& p = &a2;
p = &i; // error

mut int& p = &a2;
p = &i; // ok

char& empty; // causes error
```
## Function definition
```
int sum(int a, int b) {
    return a + b;
}
```
## Type definition
```
struct Rational {
    int num;
    int den;
}
clique {
    Rational Rational::plus(self&, Rational rhs) {
        // ...
    }
    // or
    impl Rational {
        Rational plus(self&, Rational rhs) {
            //...
        }
    }
}
```
## Preconditions
```
require { 
    a.ne(0) 
}
Rational Rational::new(int a, int b) {
    return Rational{ num = a, den = b };
}

Rational reverse1(int a) {
    return Rational::new(1, a); // error, must check a.ne(0)
}

require {
    a.ne(0)
}
Rational reverse2(int a) {
    return Rational::new(1, a); // ok
}

Result<Rational> reverse2(int a) {
    ensure (a.ne(0)) {
        return Ok(Rational::new(1, a)); // ok
    } else {
        return Err(RuntimeError::new("Cannor reverse zero"));
    }
}
```
## Traits
```
trait Printable {
    String to_string(self&);
}

String (Printable)Rational::to_string(self&) {
    return num.to_string().concat("/").concat(denom.to_string()); 
}

impl (Printable)Rational {
    // ...
}

void print(Printable& p) {
    String buf = p.to_string();
}
```
## Enumerations and templates
```
template<ResType, ErrType>
enum Result {
    Ok(ResType),
    Error(ErrType)
}
```
## Heap allocation
С этим будет дохуя проблем. Надо очень много изучить здесь. Непонятно как массив узнает сколько именно надо освободить. Непонятно как освобождать через указатель на интерфейс (узнать как в с++, или запретить?).
```
mut int& a = new int(5);
delete a;
mut int[] arr = new int[5];
delete[] arr;
```
